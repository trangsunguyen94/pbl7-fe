import i18nConfig from "./locales/config";

export default {
  ssr: false,
  server: {
    port: 3000,
    host: "localhost",
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Chat bot',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Inter:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap",
        type: 'text/css'
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap",
        type: 'text/css'
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400;1,500;1,600&display=swap",
        type: 'text/css'
      },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/scss/app.scss",
    "~/assets/scss/table.scss",
    "~/assets/scss/variables.scss",
    '~/node_modules/primevue/resources/themes/saga-blue/theme.css', // Replace with your desired theme
    '~/node_modules/primevue/resources/primevue.min.css',
    '~/node_modules/primeicons/primeicons.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/vee-validate", ssr: true },
    { src: "~/plugins/i18n", ssr: false },
    { src: "~/plugins/click-outside", ssr: false },
    { src: "~/plugins/vue-pagination", ssr: false },
    { src: "~/plugins/vue-datepicker", ssr: false },
    { src: "~/plugins/handle-error-from-api", ssr: false },
    { src: "~/plugins/helpers" },
    { src: "~plugins/vue-image-crop-upload", mode: "client", ssr: false },
    { src: "~/plugins/axios" },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    // '@nuxt/typescript-build',
    '@nuxtjs/moment',
    '@nuxtjs/pwa',
    '@nuxtjs/fontawesome'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/auth-next',
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    ['@nuxtjs/i18n', i18nConfig],
    '@nuxtjs/moment',
    '@nuxtjs/pwa',
    [
      'primevue/nuxt', {
        // theme: 'md-light-indigo',
        ripple: true,
        components: [
          'InputText',
          'Button',
          'DataTable',
          'Slider',
          'Dialog',
          'Toast',
          'TreeTable',
          'Column',
          'PickList',
          'Toolbar',
        ],
        directives: ['Tooltip', 'Badge'],
        services: ['ToastService'],
      }
    ]
  ],
  moment: {
    defaultLocale: 'en-gb',
    locales: ['vi', 'ja', 'en-gb']
  },
  pwa: {
    manifest: {
      name: 'Chat bot',
      short_name: 'VA',
      lang: "en",
    },
  },
  router: {
    middleware: ['auth']
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      compact: true
    },
    transpile: [
      'vee-validate/dist/rules',
      'defu',
      'vee-validate/dist/locale/en',
      'vee-validate/dist/locale/ja',
      'vee-validate/dist/locale/vi'
    ],
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.devtool = isClient ? 'source-map' : 'inline-source-map';
      }
    },
  },
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BASE_URL_API,
  },
  auth: {
    plugins: ['~/plugins/auth'],
    // redirect: {
    //   callback: false,
    //   login: '/login',
    //   home: '/',
    //   logout: '/login'
    // },
    watchLoggedIn: false,
    strategies: {
      local: {
        scheme: "refresh",
        endpoints: {
          login: {
            url: "/api/auth/login",
            method: "post"
          },
          user: {
            url: "/api/users/current",
            method: "get",
          },
          logout: {
            url: "/signout",
            method: "post",
          },
          refresh: {
            url: "api/auth/token/refresh",
            method: "post",
          },
        },
        user: {
          autoFetch: true,
          property: 'data',
        },
        token: {
          property: "data.accessToken",
          maxAge: 300 * 60 * 2,
          name: "x-access-token",
          type: ""
        },
        refreshToken: {
          property: 'data.refreshToken',
          data: 'refreshToken',
          maxAge: 60 * 60 * 24 * 10
        }
      },
    },
  },
  fontawesome: {
    icons: {
      solid: true,
      brands: true
    }
  }

}
