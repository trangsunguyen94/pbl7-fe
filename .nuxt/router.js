import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _17073c68 = () => interopDefault(import('../pages/account/index.vue' /* webpackChunkName: "pages/account/index" */))
const _5c211a74 = () => interopDefault(import('../pages/change-password.vue' /* webpackChunkName: "pages/change-password" */))
const _08cd0680 = () => interopDefault(import('../pages/chat-bots/index.vue' /* webpackChunkName: "pages/chat-bots/index" */))
const _60423de1 = () => interopDefault(import('../pages/forgot-password.vue' /* webpackChunkName: "pages/forgot-password" */))
const _23ae6376 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _33e6e14e = () => interopDefault(import('../pages/my-info/index.vue' /* webpackChunkName: "pages/my-info/index" */))
const _f3aa4426 = () => interopDefault(import('../pages/account/add-user.vue' /* webpackChunkName: "pages/account/add-user" */))
const _699616db = () => interopDefault(import('../pages/account/terminate.vue' /* webpackChunkName: "pages/account/terminate" */))
const _3888143e = () => interopDefault(import('../pages/chat-bots/create.vue' /* webpackChunkName: "pages/chat-bots/create" */))
const _0d697c7d = () => interopDefault(import('../pages/my-info/update.vue' /* webpackChunkName: "pages/my-info/update" */))
const _227c6048 = () => interopDefault(import('../pages/response/id/index.vue' /* webpackChunkName: "pages/response/id/index" */))
const _24812b2e = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _2f77b6b7 = () => interopDefault(import('../pages/account/_id/index.vue' /* webpackChunkName: "pages/account/_id/index" */))
const _7ac7f244 = () => interopDefault(import('../pages/intent/_id/index.vue' /* webpackChunkName: "pages/intent/_id/index" */))
const _f8195798 = () => interopDefault(import('../pages/account/_id/update.vue' /* webpackChunkName: "pages/account/_id/update" */))
const _01e5b6cf = () => interopDefault(import('../pages/chat-bots/_id/chat.vue' /* webpackChunkName: "pages/chat-bots/_id/chat" */))
const _6860d2ca = () => interopDefault(import('../pages/chat-bots/_id/contexts.vue' /* webpackChunkName: "pages/chat-bots/_id/contexts" */))
const _ba53b8d6 = () => interopDefault(import('../pages/chat-bots/_id/create-context.vue' /* webpackChunkName: "pages/chat-bots/_id/create-context" */))
const _2e5c90f0 = () => interopDefault(import('../pages/chat-bots/_id/overview.vue' /* webpackChunkName: "pages/chat-bots/_id/overview" */))
const _440bd890 = () => interopDefault(import('../pages/chat-bots/_id/response.vue' /* webpackChunkName: "pages/chat-bots/_id/response" */))
const _2d4ce613 = () => interopDefault(import('../pages/chat-bots/_id/response_create.vue' /* webpackChunkName: "pages/chat-bots/_id/response_create" */))
const _0d90644a = () => interopDefault(import('../pages/chat-bots/_id/edit-context/_contextId/index.vue' /* webpackChunkName: "pages/chat-bots/_id/edit-context/_contextId/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/account",
    component: _17073c68,
    name: "account"
  }, {
    path: "/change-password",
    component: _5c211a74,
    name: "change-password"
  }, {
    path: "/chat-bots",
    component: _08cd0680,
    name: "chat-bots"
  }, {
    path: "/forgot-password",
    component: _60423de1,
    name: "forgot-password"
  }, {
    path: "/login",
    component: _23ae6376,
    name: "login"
  }, {
    path: "/my-info",
    component: _33e6e14e,
    name: "my-info"
  }, {
    path: "/account/add-user",
    component: _f3aa4426,
    name: "account-add-user"
  }, {
    path: "/account/terminate",
    component: _699616db,
    name: "account-terminate"
  }, {
    path: "/chat-bots/create",
    component: _3888143e,
    name: "chat-bots-create"
  }, {
    path: "/my-info/update",
    component: _0d697c7d,
    name: "my-info-update"
  }, {
    path: "/response/id",
    component: _227c6048,
    name: "response-id"
  }, {
    path: "/",
    component: _24812b2e,
    name: "index"
  }, {
    path: "/account/:id",
    component: _2f77b6b7,
    name: "account-id"
  }, {
    path: "/intent/:id",
    component: _7ac7f244,
    name: "intent-id"
  }, {
    path: "/account/:id/update",
    component: _f8195798,
    name: "account-id-update"
  }, {
    path: "/chat-bots/:id?/chat",
    component: _01e5b6cf,
    name: "chat-bots-id-chat"
  }, {
    path: "/chat-bots/:id?/contexts",
    component: _6860d2ca,
    name: "chat-bots-id-contexts"
  }, {
    path: "/chat-bots/:id?/create-context",
    component: _ba53b8d6,
    name: "chat-bots-id-create-context"
  }, {
    path: "/chat-bots/:id?/overview",
    component: _2e5c90f0,
    name: "chat-bots-id-overview"
  }, {
    path: "/chat-bots/:id?/response",
    component: _440bd890,
    name: "chat-bots-id-response"
  }, {
    path: "/chat-bots/:id?/response_create",
    component: _2d4ce613,
    name: "chat-bots-id-response_create"
  }, {
    path: "/chat-bots/:id?/edit-context/:contextId",
    component: _0d90644a,
    name: "chat-bots-id-edit-context-contextId"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
