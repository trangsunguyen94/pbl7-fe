import Vue from 'vue'
import Vuex from 'vuex'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from './components/nuxt-error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'
import { createStore } from './store.js'

/* Plugins */

import nuxt_plugin_bootstrapvue_91002e42 from 'nuxt_plugin_bootstrapvue_91002e42' // Source: ./bootstrap-vue.js (mode: 'all')
import nuxt_plugin_badgepluginc5470178_135189c2 from 'nuxt_plugin_badgepluginc5470178_135189c2' // Source: ./badge.plugin.c5470178.js (mode: 'all')
import nuxt_plugin_tooltipplugin167db8a4_3e932b5b from 'nuxt_plugin_tooltipplugin167db8a4_3e932b5b' // Source: ./tooltip.plugin.167db8a4.js (mode: 'all')
import nuxt_plugin_toolbarplugin3d837e1c_5c5db392 from 'nuxt_plugin_toolbarplugin3d837e1c_5c5db392' // Source: ./toolbar.plugin.3d837e1c.js (mode: 'all')
import nuxt_plugin_picklistpluginf6d74870_030c60bc from 'nuxt_plugin_picklistpluginf6d74870_030c60bc' // Source: ./picklist.plugin.f6d74870.js (mode: 'all')
import nuxt_plugin_columnplugin5a37a31f_23704e24 from 'nuxt_plugin_columnplugin5a37a31f_23704e24' // Source: ./column.plugin.5a37a31f.js (mode: 'all')
import nuxt_plugin_treetableplugin1e18fc51_6e762455 from 'nuxt_plugin_treetableplugin1e18fc51_6e762455' // Source: ./treetable.plugin.1e18fc51.js (mode: 'all')
import nuxt_plugin_toastplugind9c7b670_4f568d5c from 'nuxt_plugin_toastplugind9c7b670_4f568d5c' // Source: ./toast.plugin.d9c7b670.js (mode: 'all')
import nuxt_plugin_dialogplugin06a48691_6bd15424 from 'nuxt_plugin_dialogplugin06a48691_6bd15424' // Source: ./dialog.plugin.06a48691.js (mode: 'all')
import nuxt_plugin_sliderplugin0107d22c_58dbe71a from 'nuxt_plugin_sliderplugin0107d22c_58dbe71a' // Source: ./slider.plugin.0107d22c.js (mode: 'all')
import nuxt_plugin_datatableplugin696f66c5_37914320 from 'nuxt_plugin_datatableplugin696f66c5_37914320' // Source: ./datatable.plugin.696f66c5.js (mode: 'all')
import nuxt_plugin_buttonplugin78ce580a_aede941c from 'nuxt_plugin_buttonplugin78ce580a_aede941c' // Source: ./button.plugin.78ce580a.js (mode: 'all')
import nuxt_plugin_inputtextplugin6aa2ec98_06ad6189 from 'nuxt_plugin_inputtextplugin6aa2ec98_06ad6189' // Source: ./inputtext.plugin.6aa2ec98.js (mode: 'all')
import nuxt_plugin_configpluginripple76d697a6_55e48b52 from 'nuxt_plugin_configpluginripple76d697a6_55e48b52' // Source: ./config.plugin-ripple.76d697a6.js (mode: 'all')
import nuxt_plugin_pluginutils_68a8698b from 'nuxt_plugin_pluginutils_68a8698b' // Source: ./nuxt-i18n/plugin.utils.js (mode: 'all')
import nuxt_plugin_pluginrouting_d3967fd4 from 'nuxt_plugin_pluginrouting_d3967fd4' // Source: ./nuxt-i18n/plugin.routing.js (mode: 'all')
import nuxt_plugin_pluginmain_22fc1319 from 'nuxt_plugin_pluginmain_22fc1319' // Source: ./nuxt-i18n/plugin.main.js (mode: 'all')
import nuxt_plugin_axios_5c34bab0 from 'nuxt_plugin_axios_5c34bab0' // Source: ./axios.js (mode: 'all')
import nuxt_plugin_fontawesome_12806c1c from 'nuxt_plugin_fontawesome_12806c1c' // Source: ./fontawesome.js (mode: 'all')
import nuxt_plugin_workbox_b87d7868 from 'nuxt_plugin_workbox_b87d7868' // Source: ./workbox.js (mode: 'client')
import nuxt_plugin_metaplugin_23affedf from 'nuxt_plugin_metaplugin_23affedf' // Source: ./pwa/meta.plugin.js (mode: 'all')
import nuxt_plugin_iconplugin_2857a453 from 'nuxt_plugin_iconplugin_2857a453' // Source: ./pwa/icon.plugin.js (mode: 'all')
import nuxt_plugin_moment_390e2ce8 from 'nuxt_plugin_moment_390e2ce8' // Source: ./moment.js (mode: 'all')
import nuxt_plugin_veevalidate_6e5ad03a from 'nuxt_plugin_veevalidate_6e5ad03a' // Source: ../plugins/vee-validate (mode: 'all')
import nuxt_plugin_i18n_6a80ea94 from 'nuxt_plugin_i18n_6a80ea94' // Source: ../plugins/i18n (mode: 'client')
import nuxt_plugin_clickoutside_2bc8eaac from 'nuxt_plugin_clickoutside_2bc8eaac' // Source: ../plugins/click-outside (mode: 'client')
import nuxt_plugin_vuepagination_16ae9f37 from 'nuxt_plugin_vuepagination_16ae9f37' // Source: ../plugins/vue-pagination (mode: 'client')
import nuxt_plugin_vuedatepicker_19e66559 from 'nuxt_plugin_vuedatepicker_19e66559' // Source: ../plugins/vue-datepicker (mode: 'client')
import nuxt_plugin_handleerrorfromapi_b036dfaa from 'nuxt_plugin_handleerrorfromapi_b036dfaa' // Source: ../plugins/handle-error-from-api (mode: 'client')
import nuxt_plugin_helpers_16d71a4f from 'nuxt_plugin_helpers_16d71a4f' // Source: ../plugins/helpers (mode: 'all')
import nuxt_plugin_vueimagecropupload_5bb82e6e from 'nuxt_plugin_vueimagecropupload_5bb82e6e' // Source: ../plugins/vue-image-crop-upload (mode: 'client')
import nuxt_plugin_axios_3566aa80 from 'nuxt_plugin_axios_3566aa80' // Source: ../plugins/axios (mode: 'all')
import nuxt_plugin_auth_ea564f00 from 'nuxt_plugin_auth_ea564f00' // Source: ./auth.js (mode: 'all')
import nuxt_plugin_auth_6a7e4e1e from 'nuxt_plugin_auth_6a7e4e1e' // Source: ../plugins/auth (mode: 'all')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Object.defineProperty(Vue.prototype, '$nuxt', {
  get() {
    const globalNuxt = this.$root ? this.$root.$options.$nuxt : null
    if (process.client && !globalNuxt && typeof window !== 'undefined') {
      return window.$nuxt
    }
    return globalNuxt
  },
  configurable: true
})

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"page","mode":"out-in","appear":true,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

const originalRegisterModule = Vuex.Store.prototype.registerModule

function registerModule (path, rawModule, options = {}) {
  const preserveState = process.client && (
    Array.isArray(path)
      ? !!path.reduce((namespacedState, path) => namespacedState && namespacedState[path], this.state)
      : path in this.state
  )
  return originalRegisterModule.call(this, path, rawModule, { preserveState, ...options })
}

async function createApp(ssrContext, config = {}) {
  const store = createStore(ssrContext)
  const router = await createRouter(ssrContext, config, { store })

  // Add this.$router into store actions/mutations
  store.$router = router

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"title":"Chat bot","htmlAttrs":{"lang":"en"},"meta":[{"charset":"utf-8"},{"name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"description","name":"description","content":""},{"name":"format-detection","content":"telephone=no"}],"link":[{"rel":"icon","type":"image\u002Fx-icon","href":"\u002Ffavicon.png"},{"rel":"stylesheet","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss2?family=Inter:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap","type":"text\u002Fcss"},{"rel":"stylesheet","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss2?family=Roboto:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap","type":"text\u002Fcss"},{"rel":"stylesheet","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400;1,500;1,600&display=swap","type":"text\u002Fcss"}],"style":[],"script":[]},

    store,
    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  // Make app available into store via this.app
  store.app = app

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    store,
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    beforeSerializeFns: ssrContext ? ssrContext.beforeSerializeFns : undefined,
    ssrContext
  })

  function inject(key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value
    // Add into context
    if (!app.context[key]) {
      app.context[key] = value
    }

    // Add into store
    store[key] = app[key]

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue.prototype, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  // Inject runtime config as $config
  inject('config', config)

  if (process.client) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }

  // Add enablePreview(previewData = {}) in context for plugins
  if (process.static && process.client) {
    app.context.enablePreview = function (previewData = {}) {
      app.previewData = Object.assign({}, previewData)
      inject('preview', previewData)
    }
  }
  // Plugin execution

  if (typeof nuxt_plugin_bootstrapvue_91002e42 === 'function') {
    await nuxt_plugin_bootstrapvue_91002e42(app.context, inject)
  }

  if (typeof nuxt_plugin_badgepluginc5470178_135189c2 === 'function') {
    await nuxt_plugin_badgepluginc5470178_135189c2(app.context, inject)
  }

  if (typeof nuxt_plugin_tooltipplugin167db8a4_3e932b5b === 'function') {
    await nuxt_plugin_tooltipplugin167db8a4_3e932b5b(app.context, inject)
  }

  if (typeof nuxt_plugin_toolbarplugin3d837e1c_5c5db392 === 'function') {
    await nuxt_plugin_toolbarplugin3d837e1c_5c5db392(app.context, inject)
  }

  if (typeof nuxt_plugin_picklistpluginf6d74870_030c60bc === 'function') {
    await nuxt_plugin_picklistpluginf6d74870_030c60bc(app.context, inject)
  }

  if (typeof nuxt_plugin_columnplugin5a37a31f_23704e24 === 'function') {
    await nuxt_plugin_columnplugin5a37a31f_23704e24(app.context, inject)
  }

  if (typeof nuxt_plugin_treetableplugin1e18fc51_6e762455 === 'function') {
    await nuxt_plugin_treetableplugin1e18fc51_6e762455(app.context, inject)
  }

  if (typeof nuxt_plugin_toastplugind9c7b670_4f568d5c === 'function') {
    await nuxt_plugin_toastplugind9c7b670_4f568d5c(app.context, inject)
  }

  if (typeof nuxt_plugin_dialogplugin06a48691_6bd15424 === 'function') {
    await nuxt_plugin_dialogplugin06a48691_6bd15424(app.context, inject)
  }

  if (typeof nuxt_plugin_sliderplugin0107d22c_58dbe71a === 'function') {
    await nuxt_plugin_sliderplugin0107d22c_58dbe71a(app.context, inject)
  }

  if (typeof nuxt_plugin_datatableplugin696f66c5_37914320 === 'function') {
    await nuxt_plugin_datatableplugin696f66c5_37914320(app.context, inject)
  }

  if (typeof nuxt_plugin_buttonplugin78ce580a_aede941c === 'function') {
    await nuxt_plugin_buttonplugin78ce580a_aede941c(app.context, inject)
  }

  if (typeof nuxt_plugin_inputtextplugin6aa2ec98_06ad6189 === 'function') {
    await nuxt_plugin_inputtextplugin6aa2ec98_06ad6189(app.context, inject)
  }

  if (typeof nuxt_plugin_configpluginripple76d697a6_55e48b52 === 'function') {
    await nuxt_plugin_configpluginripple76d697a6_55e48b52(app.context, inject)
  }

  if (typeof nuxt_plugin_pluginutils_68a8698b === 'function') {
    await nuxt_plugin_pluginutils_68a8698b(app.context, inject)
  }

  if (typeof nuxt_plugin_pluginrouting_d3967fd4 === 'function') {
    await nuxt_plugin_pluginrouting_d3967fd4(app.context, inject)
  }

  if (typeof nuxt_plugin_pluginmain_22fc1319 === 'function') {
    await nuxt_plugin_pluginmain_22fc1319(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_5c34bab0 === 'function') {
    await nuxt_plugin_axios_5c34bab0(app.context, inject)
  }

  if (typeof nuxt_plugin_fontawesome_12806c1c === 'function') {
    await nuxt_plugin_fontawesome_12806c1c(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_workbox_b87d7868 === 'function') {
    await nuxt_plugin_workbox_b87d7868(app.context, inject)
  }

  if (typeof nuxt_plugin_metaplugin_23affedf === 'function') {
    await nuxt_plugin_metaplugin_23affedf(app.context, inject)
  }

  if (typeof nuxt_plugin_iconplugin_2857a453 === 'function') {
    await nuxt_plugin_iconplugin_2857a453(app.context, inject)
  }

  if (typeof nuxt_plugin_moment_390e2ce8 === 'function') {
    await nuxt_plugin_moment_390e2ce8(app.context, inject)
  }

  if (typeof nuxt_plugin_veevalidate_6e5ad03a === 'function') {
    await nuxt_plugin_veevalidate_6e5ad03a(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_i18n_6a80ea94 === 'function') {
    await nuxt_plugin_i18n_6a80ea94(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_clickoutside_2bc8eaac === 'function') {
    await nuxt_plugin_clickoutside_2bc8eaac(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_vuepagination_16ae9f37 === 'function') {
    await nuxt_plugin_vuepagination_16ae9f37(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_vuedatepicker_19e66559 === 'function') {
    await nuxt_plugin_vuedatepicker_19e66559(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_handleerrorfromapi_b036dfaa === 'function') {
    await nuxt_plugin_handleerrorfromapi_b036dfaa(app.context, inject)
  }

  if (typeof nuxt_plugin_helpers_16d71a4f === 'function') {
    await nuxt_plugin_helpers_16d71a4f(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_vueimagecropupload_5bb82e6e === 'function') {
    await nuxt_plugin_vueimagecropupload_5bb82e6e(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_3566aa80 === 'function') {
    await nuxt_plugin_axios_3566aa80(app.context, inject)
  }

  if (typeof nuxt_plugin_auth_ea564f00 === 'function') {
    await nuxt_plugin_auth_ea564f00(app.context, inject)
  }

  if (typeof nuxt_plugin_auth_6a7e4e1e === 'function') {
    await nuxt_plugin_auth_6a7e4e1e(app.context, inject)
  }

  // Lock enablePreview in context
  if (process.static && process.client) {
    app.context.enablePreview = function () {
      console.warn('You cannot call enablePreview() outside a plugin.')
    }
  }

  // Wait for async component to be resolved first
  await new Promise((resolve, reject) => {
    // Ignore 404s rather than blindly replacing URL in browser
    if (process.client) {
      const { route } = router.resolve(app.context.route.fullPath)
      if (!route.matched.length) {
        return resolve()
      }
    }
    router.replace(app.context.route.fullPath, resolve, (err) => {
      // https://github.com/vuejs/vue-router/blob/v3.4.3/src/util/errors.js
      if (!err._isRouter) return reject(err)
      if (err.type !== 2 /* NavigationFailureType.redirected */) return resolve()

      // navigated to a different route in router guard
      const unregister = router.afterEach(async (to, from) => {
        if (process.server && ssrContext && ssrContext.url) {
          ssrContext.url = to.fullPath
        }
        app.context.route = await getRouteData(to)
        app.context.params = to.params || {}
        app.context.query = to.query || {}
        unregister()
        resolve()
      })
    })
  })

  return {
    store,
    app,
    router
  }
}

export { createApp, NuxtError }
