import moment from 'moment'

import 'moment/locale/vi'
import 'moment/locale/ja'
import 'moment/locale/en-gb'

moment.locale('en-gb')

export default (ctx, inject) => {
  ctx.$moment = moment
  inject('moment', moment)
}
