export default {
  locales: [
    {
      code: "vi",
      iso: "vi-VN",
      name: "Vietnamese",
      file: "vi.json",
    },
    {
      code: "en",
      iso: "en-EN",
      name: "English",
      file: "en.json",
    },
    {
      code: "ja",
      iso: "ja-JA",
      name: "Japanese",
      file: "ja.json",
    },
  ],

  defaultLocale: "en",
  detectBrowserLanguage: {
    useCookie: true,
    cookieKey: "my_custom_cookie_name",
    alwaysRedirect: true,
    onlyOnRoot: true,
  },
  baseUrl: "",
  seo: true,
  lazy: true,
  parsePages: false,
  vueI18n: {
    fallbackLocale: ["en", "vi", "ja"],
  },
  langDir: "locales/",
  encodePaths: false,
  strategy: 'no_prefix',
};
