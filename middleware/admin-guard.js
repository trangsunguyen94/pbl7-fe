export default ({ app, $auth, redirect }) => {
  if ($auth.loggedIn && $auth.user.role !== 'ROLE_MOD' && $auth.user.role !== 'ROLE_ADMIN') {
    redirect(app.localePath('/'));
  }
};