export default ({ app, $auth, redirect }) => {
  // if ($auth.loggedIn && $auth.user.role !== 'ROLE_USER') {
  //   redirect(app.localePath('/'));
  // }

  if ($auth.loggedIn) {
    redirect(app.localePath('/'));
  }
};