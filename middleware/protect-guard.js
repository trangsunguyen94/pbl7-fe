export default ({ app, $auth, redirect }) => {
  if ($auth.loggedIn) {
    if ($auth.user.role === 'ROLE_USER') {
      redirect(app.localePath('/info'));
    }
    return redirect(('/account'));
  }
};